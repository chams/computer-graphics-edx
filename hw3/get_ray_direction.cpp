#include <glm/glm.hpp>
#include <iostream>

#include "./coordinate_frame.h"
#include "./scene.h"
#include "./util.h"

using namespace glm;
using namespace std;

vec3 getRayDirection(const float x, const float y, const Scene* scene, const CoordinateFrame* frame) {
    Camera camera = scene->camera;
    float width = (float)scene->width;
    float height = (float)scene->height;

    float hw = width / 2;
    float hh = height / 2;

    float fovyrad = radians((float)camera.fov);
    float fovxrad = 2.0f * atan(tan(fovyrad * 0.5f) * (width / height));

    float alpha = (x - hw) / hw * tan(fovxrad / 2.0f);
    float beta = (hh - y) / hh * tan(fovyrad / 2.0f);

    vec3 direction = normalize(alpha * frame->u + beta * frame->v - frame->w);
    return direction;
}

#pragma once

#include <array>
#include <glm/glm.hpp>

struct Light {
    glm::vec4 position;
    glm::vec3 intensity;
    std::array<float, 3> attenuation;
};

#pragma once

#include "./coordinate_frame.h"
#include "./scene.h"
#include <glm/glm.hpp>

glm::vec3 getRayDirection(const float x, const float y, const Scene* scene, const CoordinateFrame* frame);

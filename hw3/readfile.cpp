#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stack>
#include <array>

#include "./objects.h"
#include "./scene.h"
#include "./transform.h"

using namespace std;
using namespace glm;

void rightMultiply(const mat4 & M, stack<mat4> & transfstack) {
    // TODO: how does it work?
    mat4 &T = transfstack.top();
    T = T * M;
}

bool readValues(stringstream &s, int numvals, float* values) {
    for (int i = 0; i < numvals; i++) {
        s >> values[i];
        if (s.fail()) {
            cout << "Failed reading value " << i << " will skip\n";
            return false;
        }
    }
    return true;
}

void readSize(stringstream &s, Scene* scene) {
    s >> scene->width;
    s >> scene->height;
}

void readCamera(stringstream &s, Scene* scene) {
    float input[10];
    readValues(s, 10, input);
    scene->camera.lookfrom = vec3(input[0], input[1], input[2]);
    scene->camera.lookat = vec3(input[3], input[4], input[5]);
    scene->camera.up = vec3(input[6], input[7], input[8]);
    scene->camera.fov = input[9];
}

void readSphere(
        stringstream &s,
        Scene* scene,
        vec3 diffuse,
        vec3 specular,
        vec3 ambient,
        vec3 emission,
        float shininess,
        mat4 transformation
        ) {
    float input[4];
    readValues(s, 4, input);
    Sphere* sphere = new Sphere(vec3(input[0], input[1], input[2]), input[3]);
    sphere->setDiffuse(diffuse);
    sphere->setSpecular(specular);
    sphere->setAmbient(ambient);
    sphere->setEmission(emission);
    sphere->setShininess(shininess);
    sphere->setTransformation(transformation);
    scene->objects.push_back(sphere);
}

void readVertex(stringstream &s, Scene* scene) {
    float input[3];
    readValues(s, 3, input);
    scene->vertex.push_back(vec3(input[0], input[1], input[2]));
}

void readTri(
        stringstream &s,
        Scene* scene,
        vec3 diffuse,
        vec3 specular,
        vec3 ambient,
        vec3 emission,
        float shininess,
        mat4 transformation
        ) {
    float input[3];
    readValues(s, 3, input);
    Triangle* triangle = new Triangle(
            scene->vertex[input[0]],
            scene->vertex[input[1]],
            scene->vertex[input[2]]
            );

    triangle->setDiffuse(diffuse);
    triangle->setSpecular(specular);
    triangle->setAmbient(ambient);
    triangle->setEmission(emission);
    triangle->setShininess(shininess);
    triangle->setTransformation(transformation);
    scene->objects.push_back(triangle);
}

void readLight(stringstream &s, Scene* scene, array<float, 3> attenuation, float w) {
    float input[6];
    readValues(s, 6, input);
    Light light = {
        .position = vec4(input[0], input[1], input[2], w),
        .intensity = vec3(input[3], input[4], input[5]),
        .attenuation = attenuation
    };
    scene->lights.push_back(light);
}

Scene readfile(const char *filename) {
    ifstream in;
    Scene scene;
    in.open(filename);
    if (in.is_open()) {
        string str, cmd;
        getline(in, str);

        float input[10];

        vec3 diffuse = vec3(0);
        vec3 specular = vec3(0);
        vec3 ambient = vec3(0.2, 0.2, 0.2);
        vec3 emission = vec3(0);
        float shininess = 1.0f;
        array<float, 3> attenuation = {1, 0, 0};

        stack<mat4> transfstack;
        transfstack.push(mat4(1.0));

        while (in) {
            // Rule out blank lines and comments
            if (str.find_first_not_of(" \t\r\n") != string::npos && str[0] != '#') {
                stringstream s(str);
                s >> cmd;

                if (cmd == "size") {
                    readSize(s, &scene);
                } else if (cmd == "maxdepth") {
                    readValues(s, 1, input);
                    scene.maxdepth = input[0];
                } else if (cmd == "output") {
                    s >> scene.output;
                    scene.output = "./output/" + scene.output;
                } else if (cmd == "camera") {
                    readCamera(s, &scene);
                } else if (cmd == "sphere") {
                    readSphere(s, &scene, diffuse, specular, ambient, emission, shininess, transfstack.top());
                } else if (cmd == "maxverts") {
                    // Vertex dynamic alloc
                } else if (cmd == "maxvertnorms") {

                } else if (cmd == "vertex") {
                    readVertex(s, &scene);
                } else if (cmd == "vertexnormal") {

                } else if (cmd == "tri") {
                    // TODO: calculate normal
                    readTri(s, &scene, diffuse, specular, ambient, emission, shininess, transfstack.top());
                } else if (cmd == "trinormal") {

                } else if (cmd == "translate") {
                    readValues(s, 3, input);
                    mat4 translate = Transform::translate(input[0], input[1], input[2]);
                    rightMultiply(translate, transfstack);
                } else if (cmd == "rotate") {
                    readValues(s, 4, input);
                    mat4 rotation = Transform::rotate(input[3], vec3(input[0], input[1], input[2]));
                    rightMultiply(rotation, transfstack);
                } else if (cmd == "scale") {
                    readValues(s, 3, input);
                    mat4 scale = Transform::scale(input[0], input[1], input[2]);
                    rightMultiply(scale, transfstack);
                } else if (cmd == "pushTransform") {
                    transfstack.push(transfstack.top());
                } else if (cmd == "popTransform") {
                    if (transfstack.size() > 1) {
                        transfstack.pop();
                    } else {
                        cerr << "Can't pop transform because stack size is 1" << "\n";
                    }
                } else if (cmd == "directional") {
                    readLight(s, &scene, attenuation, 0);
                } else if (cmd == "point") {
                    readLight(s, &scene, attenuation, 1);
                } else if (cmd == "attenuation") {
                    readValues(s, 3, input);
                    attenuation = {input[0], input[1], input[2]};
                } else if (cmd == "ambient") {
                    readValues(s, 3, input);
                    ambient[0] = input[0];
                    ambient[1] = input[1];
                    ambient[2] = input[2];
                } else if (cmd == "diffuse") {
                    readValues(s, 3, input);
                    diffuse[0] = input[0];
                    diffuse[1] = input[1];
                    diffuse[2] = input[2];
                } else if (cmd == "specular") {
                    readValues(s, 3, input);
                    specular[0] = input[0];
                    specular[1] = input[1];
                    specular[2] = input[2];
                } else if (cmd == "shininess") {
                    readValues(s, 1, input);
                    shininess = input[0];
                } else if (cmd == "emission") {
                    readValues(s, 3, input);
                    emission[0] = input[0];
                    emission[1] = input[1];
                    emission[2] = input[2];
                } else {
                    cout << "Unknown cmd: " << cmd << ", skip";
                }
            }

            getline(in, str);
        }
    }

    return scene;
}

#pragma once

#include <glm/glm.hpp>
#include <array>
#include <string>
#include <FreeImage.h>
#include "./scene.h"
#include "./coordinate_frame.h"
#include "./objects.h"

const float pi = 3.14159265;
const float lightRayEpsilon = 0.000005f;

void sceneDebugInfo(const Scene* scene);

void printVec3(const std::string label, const glm::vec3 a);
void printVec3(const glm::vec3 a);
void printMat4(const glm::mat4 M);

void printCoordinateFrame(const CoordinateFrame frame);
CoordinateFrame getCameraCoordinateFrame(const Scene* scene);

glm::vec3 calcTriangleNormal(const Triangle* triangle);
glm::vec3 transformVec3(glm::mat4 M, glm::vec3 v);
glm::vec3 transformVec3(glm::mat4 M, glm::vec3 v, bool isVector);
glm::vec3 dehomogenizeVec3(const glm::vec4 v);
std::array<BYTE, 3> vec3ToColor(glm::vec3 v);

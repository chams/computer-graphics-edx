#pragma once

#include "./objects.h"

struct Intersection {
    float t = -1;
    Object* object;
};

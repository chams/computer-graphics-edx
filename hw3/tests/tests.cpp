#include <iostream>
#include <string>
#include <glm/glm.hpp>

#include "../scene.h"
#include "../get_ray_direction.h"
#include "../coordinate_frame.h"
#include "../util.h"
#include "../objects.h"

using namespace std;
using namespace glm;

bool assertVec3Equal(const vec3 value, const vec3 should) {
    bool result = value == should;
    if (!result) {
        cout << "\n" << "Warn! Assert value == should" << "\n";
        printVec3("value", value);
        printVec3("should", should);
        cout << "\n";
    }
    return value == should;
}

bool testRayDirection(const Scene* scene, const CoordinateFrame* frame,
        const float x, const float y, const vec3 should) {
    vec3 direction = getRayDirection(x, y, scene, frame);
    return assertVec3Equal(direction, should);
}

bool testRayDirectionTop(const Scene* scene, const CoordinateFrame* frame) {
    return testRayDirection(
            scene,
            frame,
            scene->width / 2.0f,
            scene->height,
            normalize(vec3(0, -1, -1))
            );
}

bool testRayDirectionBottom(const Scene* scene, const CoordinateFrame* frame) {
    return testRayDirection(
            scene,
            frame,
            scene->width / 2.0f,
            0,
            normalize(vec3(0, 1, -1))
            );
}

bool testRayDirectionLeft(const Scene* scene, const CoordinateFrame* frame) {
    return testRayDirection(
            scene,
            frame,
            0,
            scene->height / 2,
            normalize(vec3(-1, 0, -1))
            );
}

bool testRayDirectionRight(const Scene* scene, const CoordinateFrame* frame) {
    return testRayDirection(
            scene,
            frame,
            scene->width,
            scene->height / 2,
            normalize(vec3(1, 0, -1))
            );
}

bool testRayDirectionHalfTop(const Scene* scene, const CoordinateFrame* frame) {
    return testRayDirection(
            scene,
            frame,
            scene->width / 2,
            scene->height / 4,
            normalize(vec3(0, 0.5, -1))
            );
}

bool testRayDirectionHalfBottom(const Scene* scene, const CoordinateFrame* frame) {
    return testRayDirection(
            scene,
            frame,
            scene->width / 2,
            (3 * scene->height) / 4,
            normalize(vec3(0, -0.5, -1))
            );
}

bool testRayDirectionHalfLeft(const Scene* scene, const CoordinateFrame* frame) {
    return testRayDirection(
            scene,
            frame,
            scene->width / 4,
            scene->height / 2,
            normalize(vec3(-0.5, 0, -1))
            );
}

bool testRayDirectionHalfRight(const Scene* scene, const CoordinateFrame* frame) {
    return testRayDirection(
            scene,
            frame,
            (3 * scene->width) / 4,
            scene->height / 2,
            normalize(vec3(0.5, 0, -1))
            );
}

bool testRayDirectionOrigin(const Scene* scene, const CoordinateFrame* frame) {
    const float x = scene->width / 2.0f;
    const float y = scene->height / 2.0f;

    vec3 direction = getRayDirection(x, y, scene, frame);
    bool result = assertVec3Equal(direction, vec3(0, 0, -1));
    return result;
}

bool testGetCameraCoordinateFrame(const Scene* scene) {
    CoordinateFrame frame = getCameraCoordinateFrame(scene);
    bool result = true;
    result = result && assertVec3Equal(frame.u, vec3(1, 0, 0));
    result = result && assertVec3Equal(frame.v, vec3(0, 1, 0));
    result = result && assertVec3Equal(frame.w, vec3(0, 0, 1));
    return result;
}

bool testCalcTriangleNormal() {
    Triangle triangle = Triangle(vec3(0, 0, 0), vec3(0, 0, 1), vec3(1, 0, 0));
    vec3 normal = calcTriangleNormal(&triangle);
    return assertVec3Equal(normal, vec3(0, 1, 0));
}

void runTest(const string testName, const bool testResult) {
    cout << testName << " = " << testResult << "\n---\n";
}

int main(int argc, char* argv[]) {
    Scene scene;
    scene.width = 100;
    scene.width = 100;
    scene.height = 100;
    scene.camera.lookfrom = vec3(0, 0, 1);
    scene.camera.lookat = vec3(0, 0, 0);
    scene.camera.up = vec3(0, 1, 0);
    scene.camera.fov = 90;

    CoordinateFrame frame = getCameraCoordinateFrame(&scene);

    runTest("Test getCameraCoordinateFrame", testGetCameraCoordinateFrame(&scene));

    // ADV: if you want to refine it you can add diagonal and hdiagonal tests
    runTest("Test ray direction origin", testRayDirectionOrigin(&scene, &frame));
    runTest("Test ray direction top", testRayDirectionTop(&scene, &frame));
    runTest("Test ray direction down", testRayDirectionBottom(&scene, &frame));
    runTest("Test ray direction left", testRayDirectionLeft(&scene, &frame));
    runTest("Test ray direction right", testRayDirectionRight(&scene, &frame));
    runTest("Test ray direction half top", testRayDirectionHalfTop(&scene, &frame));
    runTest("Test ray direction half bottom", testRayDirectionHalfBottom(&scene, &frame));
    runTest("Test ray direction half left", testRayDirectionHalfLeft(&scene, &frame));
    runTest("Test ray direction half right", testRayDirectionHalfRight(&scene, &frame));

    runTest("Calc triangle normal", testCalcTriangleNormal());

    return 0;
}

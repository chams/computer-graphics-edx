#pragma once

#include <glm/glm.hpp>

struct CoordinateFrame {
    glm::vec3 u;
    glm::vec3 v;
    glm::vec3 w;
};

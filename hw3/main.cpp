#include <iostream>
#include <vector>
#include <array>
#include <math.h>
#include <ctime>
#include <FreeImage.h>

#include "./readfile.h"
#include "./scene.h"
#include "./coordinate_frame.h"
#include "./util.h"
#include "./get_ray_direction.h"
#include "./intersection.h"

using namespace std;
using namespace glm;
void testPicture() {
    FreeImage_Initialise();

    int w = 100;
    int h = 100;

    BYTE *pixels = new BYTE[3 * w * h];
    for (int i = 0; i < w * h; i++) {
        pixels[i * 3 + 2] = 0xFF;
        pixels[i * 3 + 1] = 0x00;
        pixels[i * 3 + 0] = 0x00;
    }
    FIBITMAP *img = FreeImage_ConvertFromRawBits(pixels, w, h, w * 3, 24, 0xFF0000, 0x00FF00, 0x0000FF, false);
    FreeImage_Save(FIF_PNG, img, "test.png", 0);

    FreeImage_DeInitialise();
}

BYTE* createImageData(const Scene* scene) {
    return new BYTE[3 * scene->width * scene->height];
}

void saveImage(const Scene* scene, BYTE* pixels) {
    FreeImage_Initialise();

    const int w = scene->width;
    const int h = scene->height;

    FIBITMAP *img = FreeImage_ConvertFromRawBits(pixels, w, h, w * 3, 24, 0xFF0000, 0x00FF00, 0x0000FF, true);
    FreeImage_Save(FIF_PNG, img, scene->output.c_str(), 0);

    FreeImage_DeInitialise();
}

Intersection getClosestRayObjectIntersection(const Scene* scene, const vec3* rayOrigin, const vec3* rayDirection) {
    Intersection intersection;
    for (int i = 0; i < scene->objects.size(); i++) {
        Object* object = scene->objects[i];
        float t = object->intersect(*rayOrigin, *rayDirection);
        if (
                t > 0 &&
                (
                    t < intersection.t ||
                    intersection.t == -1
                )
            ) {
            intersection.t = t;
            intersection.object = object;
        }
    }

    return intersection;
}

vec3 getColor(const Scene* scene, const vec3* rayOrigin, const vec3* rayDirection, int depth) {
    if (depth <= 0) {
        return vec3(0);
    } else {
        Intersection rayObjectIntersection = getClosestRayObjectIntersection(scene, rayOrigin, rayDirection);

        if (rayObjectIntersection.t > 0) {
            vec3 diffuse = rayObjectIntersection.object->getDiffuse();
            vec3 specular = rayObjectIntersection.object->getSpecular();
            vec3 ambient = rayObjectIntersection.object->getAmbient();
            vec3 emission = rayObjectIntersection.object->getEmission();

            vec3 intersectionPoint = *rayOrigin + *rayDirection * rayObjectIntersection.t;
            vec3 intersectionNormal = rayObjectIntersection.object->getTNormal(&intersectionPoint);
            vec3 observerDirection = normalize(*rayOrigin - intersectionPoint);

            vec3 color = ambient + emission;
            for (int i = 0; i < scene->lights.size(); i++) {
                Light light = scene->lights[i];
                bool isPointLight = light.position[3] == 1;

                vec3 lightPosition = dehomogenizeVec3(light.position);
                if (isPointLight) { lightPosition -= intersectionPoint; }
                vec3 lightDirection = normalize(lightPosition);
                // only make sence for point light source
                float lightDistance = length(lightPosition);

                // TODO: define
                // fix numerical error by adding small epsilon
                // TODO: but how to choose right constant?
                vec3 lightRayOrigin = intersectionPoint + intersectionNormal * lightRayEpsilon;
                Intersection lightObjectIntersection = getClosestRayObjectIntersection(scene, &lightRayOrigin, &lightDirection);

                if (
                        lightObjectIntersection.t < 0 ||
                        (
                            isPointLight &&
                            length(lightDirection * lightObjectIntersection.t) >= lightDistance
                        )
                ) {
                    vec3 diffuseIntencity = diffuse * glm::max(dot(lightDirection, intersectionNormal), 0.0f);

                    vec3 h = normalize(lightDirection + observerDirection);
                    vec3 specularIntencity = specular * pow(glm::max(dot(h, intersectionNormal), 0.0f), rayObjectIntersection.object->getShininess());

                    // the attenuation model applies only to point lights, not directional lights
                    vec3 intensity = light.intensity;
                    // TODO: is vector?
                    if (isPointLight) {
                        float attenuation = light.attenuation[0] + light.attenuation[1] * lightDistance + light.attenuation[2] * pow(lightDistance, 2);
                        intensity = light.intensity / attenuation;
                    }

                    color = color + intensity * (diffuseIntencity + specularIntencity);
                }
            }

            vec3 reflectedRay = *rayDirection + 2.0f * dot(-1.0f * *rayDirection, intersectionNormal) * intersectionNormal;
            vec3 reflectedRayDirection = normalize(reflectedRay);
            vec3 reflectedRayOrigin = intersectionPoint + intersectionNormal * lightRayEpsilon;
            // if specular all zeros mb do not make recursive call?
            if (length(specular) >= 0) {
                color = color + specular * getColor(scene, &reflectedRayOrigin, &reflectedRayDirection, depth - 1);
            }

            return color;
        } else {
            return vec3(0);
        }
    }
}

void raytrace(const Scene* scene, BYTE* pixels) {
    CoordinateFrame frame = getCameraCoordinateFrame(scene);
    float done = 0;
    for (int i = 0; i < scene->height; i++) {
        for (int j = 0; j < scene->width; j++) {
            float x = j + 0.5;
            float y = i + 0.5;

            float nextDone = ((float)(i * scene->width + j) / (scene->height * scene->width)) * 100;
            if (nextDone - done >= 0.5) {
                cout << "Done " << nextDone << "p by " << time(nullptr) << "\n";
                done = nextDone;
            }

            vec3 rayOrigin = scene->camera.lookfrom;
            vec3 rayDirection = getRayDirection(x, y, scene, &frame);
            vec3 color = getColor(scene, &rayOrigin, &rayDirection, scene->maxdepth);
            array<BYTE, 3> colorBytes = vec3ToColor(color);

            int index = i * scene->width + j;
            pixels[index * 3 + 2] = colorBytes[0];
            pixels[index * 3 + 1] = colorBytes[1];
            pixels[index * 3 + 0] = colorBytes[2];
        }
    }
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        cerr << "Please pass sceneName argument" << "\n";

        return 1;
    } else {
        Scene scene = readfile(argv[1]);
        BYTE* pixels = createImageData(&scene);
        sceneDebugInfo(&scene);
        raytrace(&scene, pixels);
        saveImage(&scene, pixels);
        testPicture();

        return 0;
    }
}

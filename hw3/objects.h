#pragma once

#include <string>
#include <vector>
#include <glm/glm.hpp>

class Object {
    glm::vec3 diffuse = glm::vec3(0);
    glm::vec3 specular = glm::vec3(0);
    glm::vec3 ambient = glm::vec3(0);
    glm::vec3 emission = glm::vec3(0);
    glm::mat4 transformation = glm::mat4(1.0f);
    glm::mat4 inverseTransformation = glm::mat4(1.0f);
    glm::mat4 inverseTransposeTransformation = glm::mat4(1.0f);
    float shininess = 1.0f;

    protected:
    glm::vec3 n;
    glm::vec3 tN;

    public:
    virtual std::string getType() const = 0;

    virtual glm::vec3 getNormal(const glm::vec3* p) const = 0;
    virtual glm::vec3 getTNormal(const glm::vec3* tP) const = 0;
    virtual float intersect(const glm::vec3 rayOrigin, const glm::vec3 rayDirection) = 0;

    void setTransformation(glm::mat4 transformation);
    void setDiffuse(glm::vec3 nextDiffuse);
    void setSpecular(glm::vec3 nextSpecular);
    void setAmbient(glm::vec3 nextAmbient);
    void setEmission(glm::vec3 nextEmission);
    void setShininess(float nextShininess);
    glm::vec3 getDiffuse();
    glm::vec3 getSpecular();
    glm::vec3 getAmbient();
    glm::vec3 getEmission();
    float getShininess() const;
    glm::mat4 getTransformation() const;
    glm::mat4 getInverseTransformation() const;
    glm::mat4 getInverseTransposeTransformation() const;
};

class Sphere : public Object {
    glm::vec3 position;
    float radius;

    public:
    Sphere(const glm::vec3 startPosition, const float startRadius);
    std::string getType() const;
    glm::vec3 getNormal(const glm::vec3* p) const;
    glm::vec3 getTNormal(const glm::vec3* tP) const;
    float intersect(const glm::vec3 rayOrigin, const glm::vec3 rayDirection);
};

class Triangle : public Object {
    glm::vec3 a, b, c, tA, tB, tC;

    public:
    Triangle(const glm::vec3 startA, const glm::vec3 startB, const glm::vec3 startC);
    std::string getType() const;
    glm::vec3 getNormal(const glm::vec3* p) const;
    glm::vec3 getTNormal(const glm::vec3* tP) const;
    float intersect(const glm::vec3 rayOrigin, const glm::vec3 rayDirection);
    void setTransformation(glm::mat4 transformation);
    glm::vec3 getA() const;
    glm::vec3 getB() const;
    glm::vec3 getC() const;
    glm::vec3 getTA() const;
    glm::vec3 getTB() const;
    glm::vec3 getTC() const;
};

#include "./transform.h"

mat3 Transform::vecvecTranspose(const vec3& vec) {
    return mat3(
            vec[0] * vec[0], vec[0] * vec[1], vec[0] * vec[2],
            vec[1] * vec[0], vec[1] * vec[1], vec[1] * vec[2],
            vec[2] * vec[0], vec[2] * vec[1], vec[2] * vec[2]
            );
}

mat4 Transform::rotate(const float degrees, const vec3& axis) {
  // Rodrigues' rotation formula
  const float rad = glm::radians(degrees);
  const mat3 dualA = mat3(
          0.0f, axis[2], -1 * axis[1],
          -1 * axis[2], 0.0f, axis[0],
          axis[1], -1 * axis[0], 0.0f
          );
  const mat3 first = mat3(1.0f) * cos(rad);
  const mat3 second = Transform::vecvecTranspose(axis) * (1 - cos(rad));
  const mat3 third = dualA * sin(rad);
  const mat3 rotMat = first + second + third;

  return mat4(
            rotMat[0][0], rotMat[0][1], rotMat[0][2], 0,
            rotMat[1][0], rotMat[1][1], rotMat[1][2], 0,
            rotMat[2][0], rotMat[2][1], rotMat[2][2], 0,
            0, 0, 0, 1
            );
}

mat4 Transform::scale(const float &sx, const float &sy, const float &sz) {
  mat4 result = mat4(1.0f);
  result[0][0] = sx;
  result[1][1] = sy;
  result[2][2] = sz;

  return result;
}

mat4 Transform::translate(const float &tx, const float &ty, const float &tz) {
  mat4 result = mat4(1.0f);
  result[3][0] = tx;
  result[3][1] = ty;
  result[3][2] = tz;

  return result;
}

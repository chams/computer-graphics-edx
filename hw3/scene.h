#pragma once

#include <string>
#include <vector>
#include "./objects.h"
#include "./light.h"
#include <glm/glm.hpp>

struct Camera {
    glm::vec3 lookfrom;
    glm::vec3 lookat;
    glm::vec3 up;
    int fov;
};

struct Scene {
    int width;
    int height;
    Camera camera;
    std::vector<glm::vec3> vertex;
    std::vector<Object*> objects;
    std::vector<Light> lights;

    std::string output = "./output/output.png";
    int maxdepth = 5;
};

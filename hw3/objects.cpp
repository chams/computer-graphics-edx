#include <iostream>
#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <math.h>

#include "./util.h"
#include "./objects.h"

using namespace std;
using namespace glm;

void Object::setDiffuse(vec3 nextDiffuse) {
    diffuse = nextDiffuse;
}

void Object::setSpecular(vec3 nextSpecular) {
    specular = nextSpecular;
}

void Object::setAmbient(vec3 nextAmbient) {
    ambient = nextAmbient;
}

void Object::setEmission(vec3 nextEmission) {
    emission = nextEmission;
}

void Object::setShininess(float nextShininess) {
    shininess = nextShininess;
}

void Object::setTransformation(mat4 nextTransformation) {
    transformation = nextTransformation;
    inverseTransformation = inverse(transformation);
    inverseTransposeTransformation = transpose(inverseTransformation);
}

vec3 Object::getDiffuse() {
    return diffuse;
}

vec3 Object::getSpecular() {
    return specular;
}

vec3 Object::getAmbient() {
    return ambient;
}

vec3 Object::getEmission() {
    return emission;
}

float Object::getShininess() const {
    return shininess;
}

mat4 Object::getTransformation() const {
    return transformation;
}

mat4 Object::getInverseTransformation() const {
    return inverseTransformation;
}

mat4 Object::getInverseTransposeTransformation() const {
    return inverseTransposeTransformation;
}

Sphere::Sphere(const vec3 startPosition, const float startRadius) {
    position = startPosition;
    radius = startRadius;
}

string Sphere::getType() const {
    return "Sphere";
}

vec3 Sphere::getNormal(const glm::vec3* p) const {
    return normalize(*p - position);
}

vec3 Sphere::getTNormal(const glm::vec3* tP) const {
    vec3 p = transformVec3(this->getInverseTransformation(), *tP);
    vec3 normal = this->getNormal(&p);
    vec3 result = normalize(transformVec3(this->getInverseTransposeTransformation(), normal, true));
    return result;
};

float Sphere::intersect(const vec3 rayOrigin, const vec3 rayDirection) {
    vec3 tRayOrigin = transformVec3(this->getInverseTransformation(), rayOrigin);
    vec3 tRayDirection = normalize(transformVec3(this->getInverseTransformation(), rayDirection, true));
    vec3 p0MinusC = tRayOrigin - this->position;
    float a = dot(tRayDirection, tRayDirection);
    float b = 2 * dot(tRayDirection, p0MinusC);
    float c = dot(p0MinusC, p0MinusC) - pow(this->radius, 2);
    float discriminant = pow(b, 2) - 4 * a * c;
    if (discriminant >= 0) {
        float t1 = (-b + sqrt(discriminant)) / (2 * a);
        float t2 = (-b - sqrt(discriminant)) / (2 * a);
        float t;
        if (t1 < t2) {
            t = t1;
        } else {
            t = t2;
        }
        vec3 intersection = tRayOrigin + tRayDirection * t;
        intersection = transformVec3(this->getTransformation(), intersection);
        return dot(rayDirection, intersection - rayOrigin);
    } else {
        return -1;
    }
}

Triangle::Triangle(const vec3 startA, const vec3 startB, const vec3 startC) {
    a = tA = startA;
    b = tB = startB;
    c = tC = startC;

    n = tN = calcTriangleNormal(this);
}

string Triangle::getType() const {
    return "Triangle";
}

vec3 Triangle::getA() const {
    return a;
}

vec3 Triangle::getB() const {
    return b;
}

vec3 Triangle::getC() const {
    return c;
}

vec3 Triangle::getTA() const {
    return tA;
}

vec3 Triangle::getTB() const {
    return tB;
}

vec3 Triangle::getTC() const {
    return tC;
}

void Triangle::setTransformation(mat4 transformation) {
    Object::setTransformation(transformation);

    tA = transformVec3(transformation, a);
    tB = transformVec3(transformation, b);
    tC = transformVec3(transformation, c);

    tN = normalize(transformVec3(this->getInverseTransposeTransformation(), this->getNormal(&a), true));
}

vec3 Triangle::getNormal(const glm::vec3* p) const {
    return n;
}

vec3 Triangle::getTNormal(const glm::vec3* tP) const {
    return tN;
};

float Triangle::intersect(const vec3 rayOrigin, const vec3 rayDirection) {
    vec3 a = tA;
    vec3 b = tB;
    vec3 c = tC;
    // Find normal
    vec3 n = normalize(cross(c - a, b - a));
    // Find plane intersection
    float rdn = dot(rayDirection, n);
    if (rdn == 0) {
        // -1 is not a valid value for t since ray pointing only in one direction
        return -1;
    } else {
        float t = (dot(a, n) - dot(rayOrigin, n)) / rdn;
        vec3 i = rayOrigin + rayDirection * t;

        vec3 ab = b - a;
        vec3 cb = c - b;
        vec3 ac = c - a;

        vec3 ai = i - a;
        vec3 va = ab - (dot(ab, cb) / dot(cb, cb)) * cb;

        float bca = 1 - (dot(ai, va) / dot(ab, va));

        vec3 ib = b - i;
        vec3 vb = ab - (dot(ab, ac) / dot(ac, ac)) * ac;

        float bcb = 1 - (dot(ib, vb) / dot(ab, vb));

        float sum = bca + bcb;

        if (bca >= 0 && bcb >= 0 && sum <= 1) {
            return t;
        } else {
            return -1;
        }
    }
}

#include <iostream>
#include <glm/glm.hpp>

#include <string>
#include <array>
#include "./util.h"
#include "./coordinate_frame.h"
#include "./objects.h"

using namespace std;
using namespace glm;

void sceneDebugInfo(const Scene* scene) {
    cout << "width/height " << scene->width << " " << scene->height << "\n";
    cout << "camera " << scene->camera.lookfrom[0] << "\n";
    if (scene->vertex.size() > 0) {
        cout << "vertex " << scene->vertex.size() << " " << scene->vertex[0][0] << "\n";
    }
    cout << "objects size " << scene->objects.size() << "\n";
    if (scene->objects.size() > 0) {
        cout << "first object " << (scene->objects[0])->getType() << "\n";
    }
}

void printVec3(const string label, const vec3 a) {
    cout << label << " " << a[0] << "," << a[1] << "," << a[2] << "\n";
}

void printVec3(const vec3 a) {
    printVec3("vec3", a);
}

void printMat4(const mat4 M) {
    cout << M[0][0] << " " << M[1][0] << " " << M[2][0] << " " << M[3][0] << "\n";
    cout << M[0][1] << " " << M[1][1] << " " << M[2][1] << " " << M[3][1] << "\n";
    cout << M[0][2] << " " << M[1][2] << " " << M[2][2] << " " << M[3][2] << "\n";
    cout << M[0][3] << " " << M[1][3] << " " << M[2][3] << " " << M[3][3] << "\n";
}

void printCoordinateFrame(const CoordinateFrame frame) {
    cout << "u ";
    printVec3(frame.u);
    cout << "v ";
    printVec3(frame.v);
    cout << "w ";
    printVec3(frame.w);
}

CoordinateFrame getCameraCoordinateFrame(const Scene* scene) {
    Camera camera = scene->camera;
    CoordinateFrame frame;

    frame.w = normalize(camera.lookfrom - camera.lookat);
    vec3 b = normalize(camera.up);
    frame.u = normalize(cross(b, frame.w));
    frame.v = normalize(cross(frame.w, frame.u));

    return frame;
}

vec3 calcTriangleNormal(const Triangle* triangle) {
    vec3 result = cross(triangle->getC() - triangle->getA(), triangle->getC() - triangle->getB());
    return normalize(result);
}

vec3 transformVec3(mat4 M, vec3 v, bool isVector) {
    vec4 tmp = vec4(v[0], v[1], v[2], 1);
    if (isVector) { tmp[3] = 0; }
    tmp = M * tmp;
    vec3 result = vec3(tmp[0], tmp[1], tmp[2]);
    if (!isVector) {
        result[0] = result[0] / tmp[3];
        result[1] = result[1] / tmp[3];
        result[2] = result[2] / tmp[3];
    }
    return result;
}

vec3 transformVec3(mat4 M, vec3 v) {
    return transformVec3(M, v, false);
}

vec3 dehomogenizeVec3(const vec4 v) {
    if (v[3] == 0) {
        return vec3(v[0], v[1], v[2]);
    } else {
        return vec3(v[0] / v[3], v[1] / v[3], v[2] / v[3]);
    }
}

array<BYTE, 3> vec3ToColor(vec3 v) {
    array<BYTE, 3> color;
    color[0] = 255 * glm::min(v[0], 1.0f);
    color[1] = 255 * glm::min(v[1], 1.0f);
    color[2] = 255 * glm::min(v[2], 1.0f);

    return color;
}

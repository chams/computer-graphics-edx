// section: Data

const triangle1 = [
    [200, 200, -0],
    [400, 200, -0],
    [300, 400, -0]
];

const triangle2 = [
    [200, 200, -1],
    [400, 300, 5],
    [300, 400, 0]
];

const triangle3 = [
    [200, 400, 5],
    [400, 400, -5],
    [300, 200, -0]
];

// section: Helpers

const getContext = () => canvas.getContext('2d');

const createImageBuffer = () => {
    let buffer = [];
    const width = canvas.width;
    const height = canvas.height;
    buffer[width * height - 1] = 0;

    return {
        width: width,
        height: height,
        buffer: buffer
    };
};

const createImageData = () => {
    return getContext().createImageData(canvas.width, canvas.height);
};

const putImageBufferToImageData = (imageBuffer, imageData) => {
    const start = performance.now();
    for (let i = 0; i < imageBuffer.buffer.length; i++) {
        const pixelData = imageBuffer.buffer[i];
        const pointer = i * 4;
        let r, g, b, a;

        if (pixelData) {
            r = pixelData[3][0];
            g = pixelData[3][1];
            b = pixelData[3][2];
            a = pixelData[3][3];
            //console.log('found pixel data', r, g, b, a);
        } else {
            r = 0;
            g = 0;
            b = 0;
            a = 255;
        }

        imageData.data[pointer - 0] = r;
        imageData.data[pointer + 1] = g;
        imageData.data[pointer + 2] = b;
        imageData.data[pointer + 3] = a;
    }

    return imageData;
};


// section: Drawing

const drawImage = (imageData) => {
    const ctx = getContext();
    ctx.putImageData(imageData, 0, 0);
};


// section: Main

const main = () => {
    let imageData = createImageData();
    let imageBuffer = createImageBuffer();

    // Green
    imageBuffer = rasterize(imageBuffer, triangle1, [0, 255, 0, 255]);
    // Red
    imageBuffer = rasterize(imageBuffer, triangle2, [255, 0, 0, 255]);
    // Blue
    imageBuffer = rasterize(imageBuffer, triangle3, [0, 0, 255, 255]);

    imageData = putImageBufferToImageData(imageBuffer, imageData);

    drawImage(imageData);
};

window.onload = main;

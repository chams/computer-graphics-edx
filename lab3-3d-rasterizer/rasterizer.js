const findHorizontalIndicies = (triangle) => {
    const y1 = triangle[0][1];
    const y2 = triangle[1][1];
    const y3 = triangle[2][1];

    let horizontal;

    if (y1 == y2) {
      horizontal = [0, 1];
    } else if (y1 == y3) {
      horizontal = [0, 2];
    } else if (y2 == y3) {
      horizontal = [1, 2];
    } else {
      horizontal = null;
    }

    if (horizontal) {
        const x1 = triangle[horizontal[0]];
        const x2 = triangle[horizontal[1]];

        if (x1 <= x2) {
            return horizontal;
        } else {
            return [horizontal[1], horizontal[0]];
        }
    } else {
        return null;
    }
};

const findMiddleYIndex = (triangle) => {
    const ys = triangle.map((point, i) => [point[1], i])
        .sort((a, b) => a[0] - b[0]);

    return ys[1][1];
};

// @return {Tuple<Triangle, Triangle>}
const splitByHorizontal = (triangle) => {
    const middleI = findMiddleYIndex(triangle);
    const middlePoint = triangle[middleI];
    const middleY = middlePoint[1];
    const middleZ = middlePoint[2];

    let opposite = triangle.slice(0, middleI).concat(triangle.slice(middleI + 1));
    opposite = opposite[0][1] < opposite[1][1] ? opposite : [opposite[1], opposite[0]];

    const slopeXY = calcSlope(opposite[0], opposite[1]);
    const slopeZY = calcSlope(opposite[0], opposite[1], 2);
    const deltaY = middleY - opposite[0][1];

    const x = opposite[0][0] + slopeXY * deltaY;
    const z = opposite[0][2] + slopeZY * deltaY;

    const oppositePoint = [x, middleY, z];

    const triangle1 = [
        opposite[0],
        oppositePoint,
        middlePoint
    ];

    const triangle2 = [
        opposite[1],
        oppositePoint,
        middlePoint
    ];

    return [triangle1, triangle2];
};

const findTripleIdCompliment = (tuple) => {
    return 3 - (tuple[0] + tuple[1]);
};

const findSidesIndicies = (triangle, tuple) => {
    const a = findTripleIdCompliment(tuple);
    return [[tuple[0], a], [tuple[1], a]];
};

const calcSlope = (a, b, i, j) => {
    i = typeof i == 'undefined' ? 0 : i;
    j = typeof j == 'undefined' ? 1 : j;
    return (a[i] - b[i]) / (a[j] - b[j]);
};

const findClosestSubpixel = (a) => {
    return [Math.floor(a[0]) + 0.5, Math.floor(a[1]) + 0.5];
};

// section: Rasterization

const rasterize = (imageBuffer, triangle, color) => {
    const horizontalIndicies = findHorizontalIndicies(triangle);
    if (horizontalIndicies) {
        const sidesIndicies = findSidesIndicies(triangle, horizontalIndicies);
        const sideLeft = [triangle[sidesIndicies[0][0]], triangle[sidesIndicies[0][1]]];
        const sideRight = [triangle[sidesIndicies[1][0]], triangle[sidesIndicies[1][1]]];

        const slopeZYLeft = calcSlope(sideLeft[0], sideLeft[1], 2);
        const slopeZYRight = calcSlope(sideRight[0], sideRight[1], 2);

        const slopeXYLeft = calcSlope(sideLeft[0], sideLeft[1]);
        const slopeXYRight = calcSlope(sideRight[0], sideRight[1]);

        const startVertex = triangle[findTripleIdCompliment(horizontalIndicies)];
        const startYCoord = startVertex[1];
        const endYCoord = sideLeft[0][1];
        const yDirection = startYCoord <= endYCoord;
        let currentYCoord = startYCoord;

        do {
            const deltaY = currentYCoord - startYCoord;

            const startXCoord = startVertex[0] + slopeXYLeft * deltaY;
            const endXCoord = startVertex[0] + slopeXYRight * deltaY;
            let currentXCoord = startXCoord;

            const startZCoord = startVertex[2] + slopeZYLeft * deltaY;
            const endZCoord = startVertex[2] + slopeZYRight * deltaY;
            const currentZCoord = startZCoord;

            const slopeZX = calcSlope(
                [startXCoord, currentYCoord, startZCoord],
                [endXCoord, currentYCoord, endZCoord],
                2,
                0
            );

            do {
                // TODO: subpixel test
                const deltaX = currentXCoord - startXCoord;
                const currentZCoord = startZCoord + slopeZX * deltaX;

                const imageBufferIndex = Math.floor(currentYCoord) * imageBuffer.height + Math.floor(currentXCoord);
                const bufferData = imageBuffer.buffer[imageBufferIndex];

                if (!bufferData || bufferData[2] <= currentZCoord) {
                    imageBuffer.buffer[imageBufferIndex] = [
                        currentXCoord,
                        currentYCoord,
                        currentZCoord,
                        color
                    ];
                }
            } while (++currentXCoord <= endXCoord);
        } while (yDirection ? ++currentYCoord <= endYCoord : --currentYCoord >= endYCoord);

        return imageBuffer;
    } else {
        const splitted = splitByHorizontal(triangle);
        imageBuffer = rasterize(imageBuffer, splitted[0], color);
        return rasterize(imageBuffer, splitted[1], color);
    }
};

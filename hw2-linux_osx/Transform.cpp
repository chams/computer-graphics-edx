// Transform.cpp: implementation of the Transform class.

// Note: when you construct a matrix using mat4() or mat3(), it will be COLUMN-MAJOR
// Keep this in mind in readfile.cpp and display.cpp
// See FAQ for more details or if you're having problems.

#include "Transform.h"
#include <iostream>
using namespace std;

mat3 Transform::vecvecTranspose(const vec3& vec) {
    return mat3(
        vec[0] * vec[0], vec[0] * vec[1], vec[0] * vec[2],
        vec[1] * vec[0], vec[1] * vec[1], vec[1] * vec[2],
        vec[2] * vec[0], vec[2] * vec[1], vec[2] * vec[2]
            );
}

// Helper rotation function.  Please implement this.
mat3 Transform::rotate(const float degrees, const vec3& axis)
{
  // Rodrigues' rotation formula
  const float rad = glm::radians(degrees);
  const mat3 dualA = mat3(
          0.0f, axis[2], -1 * axis[1],
          -1 * axis[2], 0.0f, axis[0],
          axis[1], -1 * axis[0], 0.0f
  );
  const mat3 first = mat3(1.0f) * cos(rad);
  const mat3 second = Transform::vecvecTranspose(axis) * (1 - cos(rad));
  const mat3 third = dualA * sin(rad);
  const mat3 rotMat = first + second + third;

  return rotMat;
}

void Transform::left(float degrees, vec3& eye, vec3& up)
{
  std::cout << "Left by " << degrees << "\n";
  const mat3 rotMatrix = Transform::rotate(degrees, up);
  eye = rotMatrix * eye;
}

void Transform::up(float degrees, vec3& eye, vec3& up)
{
  std::cout << "Top by " << degrees << "\n";
  const vec3 vecZ = glm::normalize(glm::cross(eye, up));
  const mat3 rotMatrix = Transform::rotate(degrees, vecZ);
  eye = rotMatrix  * eye;
  up = rotMatrix * up;
}

mat4 Transform::lookAt(const vec3 &eye, const vec3 &center, const vec3 &up)
{
  mat4 translation = Transform::translate(-eye[0], -eye[1], -eye[2]);
  vec3 w = normalize(eye - center);
  vec3 u = normalize(cross(up, w));
  vec3 v = normalize(cross(w, u));
  mat4 rotation = mat4(1.0f);
  rotation[0][0] = u[0];
  rotation[1][0] = u[1];
  rotation[2][0] = u[2];
  rotation[0][1] = v[0];
  rotation[1][1] = v[1];
  rotation[2][1] = v[2];
  rotation[0][2] = w[0];
  rotation[1][2] = w[1];
  rotation[2][2] = w[2];
  return rotation * translation;
}

mat4 Transform::perspective(float fovy, float aspect, float zNear, float zFar)
{
  mat4 ret = mat4(1.0f);
  const float theta = glm::radians(fovy) / 2;
  const float d = 1 / tan(theta);
  ret[0][0] = 1 / aspect;
  ret[2][3] = - 1 / d;
  ret[3][3] = 0;
  ret = ret * d;
  // TODO: investigate
  ret[2][2] = ((zNear + zFar) / (zNear - zFar));
  ret[3][2] = ((2 * zNear * zFar) / (zNear - zFar));
  return ret;
}

mat4 Transform::scale(const float &sx, const float &sy, const float &sz)
{
  mat4 result = mat4(1.0f);
  result[0][0] = sx;
  result[1][1] = sy;
  result[2][2] = sz;

  return result;
}

mat4 Transform::translate(const float &tx, const float &ty, const float &tz)
{
  mat4 result = mat4(1.0f);
  result[3][0] = tx;
  result[3][1] = ty;
  result[3][2] = tz;

  return result;
}

// To normalize the up direction and construct a coordinate frame.
// As discussed in the lecture.  May be relevant to create a properly
// orthogonal and normalized up.
// This function is provided as a helper, in case you want to use it.
// Using this function (in readfile.cpp or display.cpp) is optional.

vec3 Transform::upvector(const vec3 &up, const vec3 & zvec)
{
  vec3 x = glm::cross(up,zvec);
  vec3 y = glm::cross(zvec,x);
  vec3 ret = glm::normalize(y);
  return ret;
}


Transform::Transform()
{

}

Transform::~Transform()
{

}

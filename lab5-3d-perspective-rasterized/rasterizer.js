const findHorizontalIndicies = (triangle) => {
    const y1 = triangle[0][1];
    const y2 = triangle[1][1];
    const y3 = triangle[2][1];

    let horizontal;

    if (y1 == y2) {
      horizontal = [0, 1];
    } else if (y1 == y3) {
      horizontal = [0, 2];
    } else if (y2 == y3) {
      horizontal = [1, 2];
    } else {
      horizontal = null;
    }

    if (horizontal) {
        const x1 = triangle[horizontal[0]][0];
        const x2 = triangle[horizontal[1]][0];

        if (x1 <= x2) {
            return horizontal;
        } else {
            return [horizontal[1], horizontal[0]];
        }
    } else {
        return null;
    }
};

const findMiddleYIndex = (triangle) => {
    const ys = triangle.map((point, i) => [point[1], i])
        .sort((a, b) => a[0] - b[0]);

    return ys[1][1];
};

// @return {Tuple<Triangle, Triangle>}
const splitByHorizontal = (triangle, unprojected, color) => {
    const middleI = findMiddleYIndex(triangle);

    // TODO: cut z interpolation problem
    const cut = (shape) => {
        const middlePoint = shape[middleI];
        const middleY = middlePoint[1];
        let opposite = shape.slice(0, middleI).concat(shape.slice(middleI + 1));
        opposite = opposite[0][1] < opposite[1][1] ? opposite : [opposite[1], opposite[0]];

        const deltaY = middleY - opposite[0][1];
        const deltaYNorm = deltaY / (opposite[1][1] - opposite[0][1]);
        const x = lerp(opposite[1][0], opposite[0][0], deltaYNorm);
        const z = lerp(opposite[1][2], opposite[0][2], deltaYNorm);

        const oppositePoint = [x, middleY, z];

        const part1 = [
            opposite[0],
            oppositePoint,
            middlePoint
        ];

        const part2 = [
            opposite[1],
            oppositePoint,
            middlePoint
        ];

        return [part1, part2];
    };

    return [cut(triangle), cut(unprojected)];
};

const findTripleIdCompliment = (tuple) => {
    return 3 - (tuple[0] + tuple[1]);
};

const findSidesIndicies = (triangle, tuple) => {
    const a = findTripleIdCompliment(tuple);
    return [[tuple[0], a], [tuple[1], a]];
};

const findClosestSubpixel = (a) => {
    return [Math.floor(a[0]) + 0.5, Math.floor(a[1]) + 0.5];
};

const interpolateZ = (z1, z2, s) => {
    const ztReciprocal = 1 / z1 + s * (1 / z2 - 1 / z1);

    return 1 / ztReciprocal;
};

const lerp = (a, b, s) => {
    return a * s + b * (1 - s);
};

// section: Rasterization

const rasterize = (imageBuffer, projected, color, unprojected) => {
    const horizontalIndicies = findHorizontalIndicies(projected);
    if (horizontalIndicies) {
        const sidesIndicies = findSidesIndicies(projected, horizontalIndicies);
        const sideLeftIndices = sidesIndicies[0];
        const sideRightIndices = sidesIndicies[1];
        const startVertexIndex = findTripleIdCompliment(horizontalIndicies);

        const pixelSideLeft = [projected[sideLeftIndices[0]], projected[sideLeftIndices[1]]];
        const pixelSideRight = [projected[sideRightIndices[0]], projected[sideRightIndices[1]]];
        const pixelStartVertex = projected[startVertexIndex];
        const pixelStartYCoord = pixelStartVertex[1];
        const pixelEndYCoord = pixelSideLeft[0][1];
        const pixelMinYCoord = Math.min(pixelStartYCoord, pixelEndYCoord);
        const pixelMaxYCoord = Math.max(pixelStartYCoord, pixelEndYCoord);
        let pixelCurrentYCoord = pixelMinYCoord;

        const modelSideLeft = [unprojected[sideLeftIndices[0]], unprojected[sideLeftIndices[1]]];
        const modelSideRight = [unprojected[sideRightIndices[0]], unprojected[sideRightIndices[1]]];

        do {
            const deltaYNorm = (pixelCurrentYCoord - pixelMinYCoord) / (pixelMaxYCoord - pixelMinYCoord);

            let pixelStartXCoord, pixelEndXCoord;
            if (pixelStartYCoord < pixelEndYCoord) {
                pixelStartXCoord = lerp(pixelSideLeft[0][0], pixelSideLeft[1][0], deltaYNorm);
                pixelEndXCoord = lerp(pixelSideRight[0][0], pixelSideRight[1][0], deltaYNorm);
            } else {
                pixelStartXCoord = lerp(pixelSideLeft[1][0], pixelSideLeft[0][0], deltaYNorm);
                pixelEndXCoord = lerp(pixelSideRight[1][0], pixelSideRight[0][0], deltaYNorm);
            }
            let pixelCurrentXCoord = pixelStartXCoord;

            let modelStartZCoord, modelEndZCoord;
            if (pixelStartYCoord < pixelEndYCoord) {
                modelStartZCoord = interpolateZ(modelSideLeft[1][2], modelSideLeft[0][2], deltaYNorm);
                modelEndZCoord = interpolateZ(modelSideRight[1][2], modelSideRight[0][2], deltaYNorm);
            } else {
                modelStartZCoord = interpolateZ(modelSideLeft[0][2], modelSideLeft[1][2], deltaYNorm);
                modelEndZCoord = interpolateZ(modelSideRight[0][2], modelSideRight[1][2], deltaYNorm);
            }

            do {
                const deltaXNorm = (pixelCurrentXCoord - pixelStartXCoord) / (pixelEndXCoord - pixelStartXCoord);

                const modelCurrentZCoord = interpolateZ(modelStartZCoord, modelEndZCoord, deltaXNorm);

                const imageBufferIndex = Math.round(pixelCurrentYCoord) * imageBuffer.height + Math.round(pixelCurrentXCoord);
                const bufferData = imageBuffer.buffer[imageBufferIndex];

                if (!bufferData || bufferData[2] > modelCurrentZCoord) {
                    imageBuffer.buffer[imageBufferIndex] = [
                        0,
                        0,
                        modelCurrentZCoord,
                        color
                    ];
                }
            } while (++pixelCurrentXCoord <= pixelEndXCoord);

        } while (++pixelCurrentYCoord <= pixelMaxYCoord);

        return imageBuffer;
    } else {
        const splitted = splitByHorizontal(projected, unprojected, color);
        imageBuffer = rasterize(imageBuffer, splitted[0][0], color, splitted[1][0]);
        imageBuffer = rasterize(imageBuffer, splitted[0][1], color, splitted[1][1]);
        return imageBuffer;
    }
};

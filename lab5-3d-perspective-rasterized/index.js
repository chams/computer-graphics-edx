// section: Tasks

// TODO: retina
// TODO: bug with lines
// TODO: bug with disapearence


// section: Helpers

const deg2rad = (deg) => (deg / 180) * Math.PI;

const getContext = () => canvas.getContext('2d');

const dehomoginizeTriangle = (triangle) => {
    let result = [];
    for (const vector of triangle) {
        result.push(dehomoginize(vector));
    }

    return result;
};


// section: Data

const AXIS = ['x', 'y', 'z'];

const faceA = [
    math.matrix([0, 0, 0, 1]),
    math.matrix([1, 0, 0, 1]),
    math.matrix([0, 1, 0, 1])
];

const faceB = [
    math.matrix([0, 0, 0, 1]),
    math.matrix([0, 0, 1, 1]),
    math.matrix([0, 1, 0, 1])
];

const faceC = [
    math.matrix([0, 0, 1, 1]),
    math.matrix([1, 0, 0, 1]),
    math.matrix([0, 1, 0, 1])
];

const faceD = [
    math.matrix([0, 0, 0, 1]),
    math.matrix([1, 0, 0, 1]),
    math.matrix([0, 0, 1, 1])
];

const faceDA = [
    math.matrix([0, 0, 0, 1]),
    math.matrix([1, 0, 0, 1]),
    math.matrix([0, 1, 0, 1])
];

const faceDB = [
    math.matrix([0, 0, 0, 1]),
    math.matrix([0, 0, 1, 1]),
    math.matrix([0, 1, 0, 1])
];

const faceDC = [
    math.matrix([0, 0, 1, 1]),
    math.matrix([1, 0, 0, 1]),
    math.matrix([0, 1, 0, 1])
];

const faceDD = [
    math.matrix([0, 0, 0, 1]),
    math.matrix([1, 0, 0, 1]),
    math.matrix([0, 0, 1, 1])
];


// section: State management

let state = {
    rotation: [0, 0, 0],
    translation: [0, 0, 0],
    scale: [0, 0, 0],
    projectionD: 0
};

let updateState = (rotation, translation, scale, projectionD) => {
    state = {
        rotation: rotation || state.rotation,
        translation: translation || state.translation,
        scale: scale || state.scale,
        projectionD: typeof projectionD != 'undefined' ? projectionD : state.projectionD
    }

    return state;
};

// section: Transformation

const getTranslationMat = (tx, ty, tz) => {
    return math.matrix([
        [1, 0, 0, tx],
        [0, 1, 0, ty],
        [0, 0, 1, tz],
        [0, 0, 0, 1]
    ]);
};

const getIdMat = () => {
    return math.matrix([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ]);
};

const getScaleMat = (sx, sy, sz) => {
    return math.matrix([
        [sx, 0, 0, 0],
        [0, sy, 0, 0],
        [0, 0, sz, 0],
        [0, 0, 0, 1]
    ]);
};

const getZRotationMat = (deg) => {
    const rad = deg2rad(deg);
    return math.matrix([
        [Math.cos(rad), -Math.sin(rad), 0, 0],
        [Math.sin(rad), Math.cos(rad), 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ]);
};

const getXRotationMat = (deg) => {
    const rad = deg2rad(deg);
    return math.matrix([
        [1, 0, 0, 0],
        [0, Math.cos(rad), -Math.sin(rad), 0],
        [0, Math.sin(rad), Math.cos(rad), 0],
        [0, 0, 0, 1]
    ]);
};

const getYRotationMat = (deg) => {
    const rad = deg2rad(deg);
    return math.matrix([
        [Math.cos(rad), 0, -Math.sin(rad), 0],
        [0, 1, 0, 0],
        [Math.sin(rad), 0, Math.cos(rad), 0],
        [0, 0, 0, 1]
    ]);
};

const transformFace = (face, mat) => {
    return face.map((point) => math.multiply(mat, point));
};

const dehomoginize = (vector) => {
    const array = vector.toArray();
    return [
        array[0] / array[3],
        array[1] / array[3],
        array[2] / array[3]
    ];
};

const roundTrianglePixels = (triangle) => {
    return triangle.map((face) => {
        return face.map((coord) => Math.round(coord));
    });
};

const getTransformMatFromState = () => {
    let translation = getTranslationMat.apply(this, state.translation);
    let scale = getScaleMat.apply(this, state.scale);
    let rotationX = getXRotationMat(state.rotation[0]);
    let rotationY = getYRotationMat(state.rotation[1]);
    let rotationZ = getZRotationMat(state.rotation[2]);
    let rotation = math.multiply(rotationZ, rotationY, rotationX);

    return math.multiply(translation, rotation, scale);
};


// section: Drawing

// TODO: debug
const drawFace = (face, color) => {
    const ctx = getContext();

    ctx.fillStyle = color;
    ctx.strokeStyle = 'white';
    ctx.beginPath();
    let start;
    for (const vector of face) {
        const point = dehomoginize(vector);
        start = start || point;
        const x = point[0], y = point[1];
        ctx.lineTo(x, y);
        //ctx.arc(x, y, 2, 0, 2 * Math.PI);
    }
    ctx.lineTo(start[0], start[1]);
    //ctx.fill();
    ctx.stroke();
    ctx.closePath();
    ctx.fillStyle = 'black';
};


// section: View

const getInput = (name) => document.querySelector(`input[name=${name}]`);

const getTranslationInput = (axis) => getInput(`translation${axis.toUpperCase()}`);

const getRotationInput = (axis) => getInput(`rotation${axis.toUpperCase()}`);

const getScaleInput = (axis) => getInput(`scale${axis.toUpperCase()}`);

const getProjectionDInput = () => {
    return getInput('projectionD');
};

const getTranslationValue = () => {
    const x = getTranslationInput('x').value;
    const y = getTranslationInput('y').value;
    const z = getTranslationInput('z').value;

    return [x, y, z];
};

const getRotationValue = () => {
    const x = getRotationInput('x').value;
    const y = getRotationInput('y').value;
    const z = getRotationInput('z').value;

    return [x, y, z];
};

const getScaleValue = () => {
    const x = getScaleInput('x').value;
    const y = getScaleInput('y').value;
    const z = getScaleInput('z').value;

    return [x, y, z];
};


const syncViewWithState = () => {
    for (const i in AXIS) {
        const currentAxis = AXIS[i];
        getScaleInput(currentAxis).value = state.scale[i];
        getTranslationInput(currentAxis).value = state.translation[i];
        getRotationInput(currentAxis).value = state.rotation[i];
    }

    getProjectionDInput().value = state.projectionD;
};

const onInputChange = (input, i, stateValue, j) => {
    const value = +input.value;
    let nextValue = stateValue;
    nextValue[i] = value;
    let arg = [undefined, undefined, undefined];
    arg[j] = nextValue;
    updateState.apply(this, arg);
    redraw();
};

const listenForm = () => {
    for (const i in AXIS) {
        const axis = AXIS[i];

        const scaleInput = getScaleInput(axis);
        scaleInput.addEventListener('change', (e) => {
            onInputChange(scaleInput, i, getScaleValue(axis), 2);
        });

        const translationInput = getTranslationInput(axis);
        translationInput.addEventListener('change', (e) => {
            onInputChange(translationInput, i, getTranslationValue(axis), 1);
        });

        const rotationInput = getRotationInput(axis);
        rotationInput.addEventListener('change', (e) => {
            onInputChange(rotationInput, i, getRotationValue(axis), 0);
        });
    }

    getProjectionDInput().addEventListener('change', (e) => {
        updateState(undefined, undefined, undefined, +getProjectionDInput().value);
        redraw();
    });
};

const drawDummy = (viewing, projection, imageBuffer, rX, rY, rZ) => {
    rX = typeof rX != 'undefined' ? rX : 45;
    rY = typeof rY != 'undefined' ? rY : 75;
    rZ = typeof rZ != 'undefined' ? rZ : 45;
    const translation = getTranslationMat(-50, 0, 1500);
    const scale = getScaleMat(100, 100, 100);
    const rotationX = getXRotationMat(rX);
    const rotationY = getYRotationMat(rY);
    const rotationZ = getZRotationMat(rZ);
    const rotation = math.multiply(rotationZ, rotationY, rotationX);

    let transform = math.multiply(translation, rotation, scale);
    let projectedTransform = math.multiply(projection, transform);

    transform = math.multiply(viewing, transform);
    projectedTransform = math.multiply(viewing, projectedTransform);

    let modelviewA = transformFace(faceDA, projectedTransform);
    let modelviewB = transformFace(faceDB, projectedTransform);
    let modelviewC = transformFace(faceDC, projectedTransform);
    let modelviewD = transformFace(faceDD, projectedTransform);

    let unprojectedA = dehomoginizeTriangle(transformFace(faceDA, transform));
    let unprojectedB = dehomoginizeTriangle(transformFace(faceDB, transform));
    let unprojectedC = dehomoginizeTriangle(transformFace(faceDC, transform));
    let unprojectedD = dehomoginizeTriangle(transformFace(faceDD, transform));

    imageBuffer = rasterize(imageBuffer, roundTrianglePixels(dehomoginizeTriangle(modelviewA)), [255, 255, 0, 255], unprojectedA);
    imageBuffer = rasterize(imageBuffer, roundTrianglePixels(dehomoginizeTriangle(modelviewB)), [0, 255, 255, 255], unprojectedB);
    imageBuffer = rasterize(imageBuffer, roundTrianglePixels(dehomoginizeTriangle(modelviewC)), [255, 0, 255, 255], unprojectedC);
    imageBuffer = rasterize(imageBuffer, roundTrianglePixels(dehomoginizeTriangle(modelviewD)), [0, 255, 0, 255], unprojectedD);

    return imageBuffer;
};

const redraw = (dummyRX, dummyRY, dummyRZ) => {
    getContext().clearRect(0, 0, canvas.width, canvas.height);

    let transform = getTransformMatFromState();

    let projection = math.matrix([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 1/state.projectionD, 0]
    ]);
    let projectedTransform = math.multiply(projection, transform);

    const viewing = math.matrix([
        [1, 0, 0, canvas.width / 2],
        [0, 1, 0, canvas.height / 2],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ]);

    projectedTransform = math.multiply(viewing, projectedTransform);
    transform = math.multiply(viewing, transform);

    let modelviewA = transformFace(faceA, projectedTransform);
    let modelviewB = transformFace(faceB, projectedTransform);
    let modelviewC = transformFace(faceC, projectedTransform);
    let modelviewD = transformFace(faceD, projectedTransform);

    let modelviewAUntransformed = dehomoginizeTriangle(transformFace(faceA, transform));
    let modelviewBUntransformed = dehomoginizeTriangle(transformFace(faceB, transform));
    let modelviewCUntransformed = dehomoginizeTriangle(transformFace(faceC, transform));
    let modelviewDUntransformed = dehomoginizeTriangle(transformFace(faceD, transform));

    syncViewWithState();
    let imageData = createImageData();
    let imageBuffer = createImageBuffer();

    imageBuffer = drawDummy(viewing, projection, imageBuffer, dummyRX, dummyRY, dummyRZ);

    imageBuffer = rasterize(imageBuffer, roundTrianglePixels(dehomoginizeTriangle(modelviewA)), [255, 0, 0, 255], modelviewAUntransformed);
    imageBuffer = rasterize(imageBuffer, roundTrianglePixels(dehomoginizeTriangle(modelviewB)), [0, 255, 0, 255], modelviewBUntransformed);
    imageBuffer = rasterize(imageBuffer, roundTrianglePixels(dehomoginizeTriangle(modelviewC)), [0, 0, 255, 255], modelviewCUntransformed);
    imageBuffer = rasterize(imageBuffer, roundTrianglePixels(dehomoginizeTriangle(modelviewD)), [255, 0, 255, 255], modelviewDUntransformed);
    imageData = putImageBufferToImageData(imageBuffer, imageData);
    drawImage(imageData);

    //drawFace(modelviewA);
    //drawFace(modelviewB);
    //drawFace(modelviewC);
};


// section: Main

const amazeMe = () => {
    let nextRotation = state.rotation;
    let nextScale = state.scale;
    let nextTranslation = state.translation;

    const randomize = (triple) => {
        let result = [];
        for (const value of triple) {
            result.push(value + Math.random() * (Math.random() < 0.5 ? -1 : 1));
        }
        return result;
    };

    const loop = () => {
        nextRotation = [(nextRotation[0] + 1) % 360, (nextRotation[1] - 1) % 360, (nextRotation[2] + 1) % 360];
        //nextScale = randomize(nextScale);
        //nextTranslation = randomize(nextTranslation);
        updateState(nextRotation, nextTranslation, nextScale);
        syncViewWithState();
        redraw();

        setTimeout(loop, 1000 / 30);
    };

    loop();
};

const circular = () => {
    const d = 1500;
    const r = 300;

    const timeline = r * 2;

    const getNextZ = (x) => {
        return d + Math.sin(x * Math.PI * (1 / r)) * r;
    };

    let currentTime = 0;
    const timelineInc = 5;
    const xInc = timelineInc;
    let vector = 1;
    let nextX = -r;
    let nextZ = d;
    let nextRX = 0, nextRY = 0, nextRZ = 0;

    const loop = () => {
        currentTime = (currentTime + timelineInc) % timeline;

        if (currentTime < timeline / 2) {
            vector = 1;
        } else {
            vector = -1;
        }

        nextX += xInc * vector;
        nextZ = getNextZ(currentTime);

        nextRX = 360 * (currentTime / timeline);
        nextRY = 360 * (currentTime / timeline);
        nextRZ = 360 * (currentTime / timeline);

        updateState([nextRX, nextRY, nextRZ], [nextX, 0, nextZ], null);
        syncViewWithState();
        redraw(nextRX, nextRY, 0);

        setTimeout(loop, 1000 / 30);
    };

    loop();
};

const main = () => {
    updateState([0, 0, 0], [0, 0, 1000], [100, 100, 100], 1000);
    //updateState([135, 142, 0], [0, 0, 1000], [100, 100, 100], 1000);
    //updateState([2, 72, 12], [200, 200, 0], [100, 100, 100]);
    listenForm();
    //amazeMe();
    circular();
    redraw();
};

window.onload = main;

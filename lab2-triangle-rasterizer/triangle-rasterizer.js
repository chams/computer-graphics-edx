// section: Data

const basicTriangle = [
    [200, 200],
    [400, 200],
    [300, 400]
];

const triangleWithoutHorizontal = [
    [200, 200],
    [400, 300],
    [300, 400]
];


// section: Helpers

const getContext = () => canvas.getContext('2d');

const findHorizontal = (triangle) => {
    const y1 = triangle[0][1];
    const y2 = triangle[1][1];
    const y3 = triangle[2][1];

    if (y1 == y2) {
      return [0, 1];
    } else if (y1 == y3) {
      return [0, 2];
    } else if (y2 == y3) {
      return [1, 2];
    } else {
      return null;
    }
};

const findMiddleYIndex = (triangle) => {
    const ys = triangle.map((point, i) => [point[1], i])
        .sort((a, b) => a[0] - b[0]);

    return ys[1][1];
};

// @return {Triangle[]}
const splitByHorizontal = (triangle) => {
    const middleI = findMiddleYIndex(triangle);
    const opposite = triangle.slice(0, middleI).concat(triangle.slice(middleI + 1));
    const slope = calcSlope(opposite[0], opposite[1]);
    const middleY = triangle[middleI][1];
    // TODO: ordering
    const deltaY = opposite[0][1] - middleY;
    const x = opposite[0][0] - slope * deltaY;

    const triangle1 = [
        opposite[0],
        [x, middleY],
        triangle[middleI]
    ];

    const triangle2 = [
        opposite[1],
        [x, middleY],
        triangle[middleI]
    ];

    return [triangle1, triangle2];
};

const findTripleIdCompliment = (tuple) => {
    return 3 - (tuple[0] + tuple[1]);
};

const findOpposites = (triangle, tuple) => {
    const a = findTripleIdCompliment(tuple);
    return [[tuple[0], a], [tuple[1], a]];
};

const calcSlope = (a, b) => {
    return (a[0] - b[0]) / (a[1] - b[1]);
};

const findClosestSubpixel = (a) => {
    return [Math.floor(a[0]) + 0.5, Math.floor(a[1]) + 0.5];
};


// section: Draw

const drawOutline = (triangle) => {
    const ctx = getContext();
    const initialStrokeStyle = ctx.strokeStyle;
    let first;

    ctx.strokeStyle = 'lightgray';
    ctx.beginPath();
    for (const point of triangle) {
        first = first || point;
        const x = point[0], y = point[1];
        ctx.lineTo(x, y);
    }
    ctx.lineTo(first[0], first[1]);
    ctx.stroke();
    ctx.closePath();
    ctx.strokeStyle = initialStrokeStyle;
};

const drawSubpixel = (a, color) => {
  const coord = [Math.floor(a[0]), Math.floor(a[1])];
    const ctx = getContext();
    ctx.fillStyle = color || 'blue';
    ctx.beginPath();
    ctx.rect(coord[0], coord[1], 1, 1);
    ctx.fill();
    ctx.closePath();
};


// section: Rasterization

const rasterize = (triangle, color) => {
    const horizontal = findHorizontal(triangle);
    if (horizontal) {
        const opposites = findOpposites(triangle, horizontal);
        const opposite1 = opposites[0];
        const opposite2 = opposites[1];
        const slope1 = calcSlope(triangle[opposite1[0]], triangle[opposite1[1]]);
        const slope2 = calcSlope(triangle[opposite2[0]], triangle[opposite2[1]]);
        const startVertex = triangle[findTripleIdCompliment(horizontal)];
        const endYCoord = triangle[horizontal[0]][1];
        let currentYCoord = startVertex[1];

        const next = () => {
            if (startVertex[1] <= endYCoord) {
                return ++currentYCoord <= endYCoord;
            } else {
                return --currentYCoord >= endYCoord;
            }
        }

        do {
            const deltaY = currentYCoord - startVertex[1];
            let currentXCoord = startVertex[0] + slope1 * deltaY;
            const endXCoord = startVertex[0] + slope2 * deltaY;

            do {
                // TODO: X ordering
                const subpixel = findClosestSubpixel([currentXCoord, currentYCoord]);
                drawSubpixel(subpixel, color);
            } while (++currentXCoord <= endXCoord);

        } while (next());
      } else {
          const splitted = splitByHorizontal(triangle);
          rasterize(splitted[0]);
          rasterize(splitted[1]);
      }
};


// section: Main

const main = () => {
    rasterize(basicTriangle, 'green');
    //drawOutline(basicTriangle);
    rasterize(triangleWithoutHorizontal, 'blue');
    //drawOutline(triangleWithoutHorizontal);
};

window.onload = main;


// section: Tasks

// TODO: retina
// TODO: bug with lines
// TODO: bug with disapearence


// section: Helpers

const deg2rad = (deg) => (deg / 180) * Math.PI;

const getContext = () => canvas.getContext('2d');

const dehomoginizeTriangle = (triangle) => {
    let result = [];
    for (const vector of triangle) {
        result.push(dehomoginize(vector));
    }

    return result;
};


// section: Data

const AXIS = ['x', 'y', 'z'];

const faceA = [
    math.matrix([0, 0, 0, 1]),
    math.matrix([1, 0, 0, 1]),
    math.matrix([0, 1, 0, 1])
];

const faceB = [
    math.matrix([0, 0, 0, 1]),
    math.matrix([0, 0, 1, 1]),
    math.matrix([0, 1, 0, 1])
];

const faceC = [
    math.matrix([0, 0, 1, 1]),
    math.matrix([1, 0, 0, 1]),
    math.matrix([0, 1, 0, 1])
];

const faceD = [
    math.matrix([0, 0, 0, 1]),
    math.matrix([1, 0, 0, 1]),
    math.matrix([0, 0, 1, 1])
];


// section: State management

let state = {
    rotation: [0, 0, 0],
    translation: [0, 0, 0],
    scale: [0, 0, 0]
};

let updateState = (rotation, translation, scale) => {
    state = {
        rotation: rotation || state.rotation,
        translation: translation || state.translation,
        scale: scale || state.scale
    }

    return state;
};

// section: Transformation

const getTranslationMat = (tx, ty, tz) => {
    return math.matrix([
        [1, 0, 0, tx],
        [0, 1, 0, ty],
        [0, 0, 1, tz],
        [0, 0, 0, 1]
    ]);
};

const getIdMat = () => {
    return math.matrix([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ]);
};

const getScaleMat = (sx, sy, sz) => {
    return math.matrix([
        [sx, 0, 0, 0],
        [0, sy, 0, 0],
        [0, 0, sz, 0],
        [0, 0, 0, 1]
    ]);
};

const getZRotationMat = (deg) => {
    const rad = deg2rad(deg);
    return math.matrix([
        [Math.cos(rad), -Math.sin(rad), 0, 0],
        [Math.sin(rad), Math.cos(rad), 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ]);
};

const getXRotationMat = (deg) => {
    const rad = deg2rad(deg);
    return math.matrix([
        [1, 0, 0, 0],
        [0, Math.cos(rad), -Math.sin(rad), 0],
        [0, Math.sin(rad), Math.cos(rad), 0],
        [0, 0, 0, 1]
    ]);
};

const getYRotationMat = (deg) => {
    const rad = deg2rad(deg);
    return math.matrix([
        [Math.cos(rad), 0, -Math.sin(rad), 0],
        [0, 1, 0, 0],
        [Math.sin(rad), 0, Math.cos(rad), 0],
        [0, 0, 0, 1]
    ]);
};

const transformFace = (face, mat) => {
    return face.map((point) => math.multiply(mat, point));
};

const dehomoginize = (vector) => {
    const array = vector.toArray();
    return [
        array[0] / array[3],
        array[1] / array[3],
        array[2] / array[3]
    ];
};

const getTransformMatFromState = () => {
    let translation = getTranslationMat.apply(this, state.translation);
    let scale = getScaleMat.apply(this, state.scale);
    let rotationX = getXRotationMat(state.rotation[0]);
    let rotationY = getYRotationMat(state.rotation[1]);
    let rotationZ = getZRotationMat(state.rotation[2]);
    let rotation = math.multiply(rotationZ, rotationY, rotationX);

    return math.multiply(translation, rotation, scale);
};


// section: Drawing

// TODO: debug
const drawFace = (face, color) => {
    const ctx = getContext();

    ctx.fillStyle = color;
    ctx.strokeStyle = 'white';
    ctx.beginPath();
    let start;
    for (const vector of face) {
        const point = dehomoginize(vector);
        start = start || point;
        //console.log('draw', JSON.stringify(point));
        const x = point[0], y = point[1];
        ctx.lineTo(x, y);
        //ctx.arc(x, y, 2, 0, 2 * Math.PI);
    }
    ctx.lineTo(start[0], start[1]);
    //ctx.fill();
    ctx.stroke();
    ctx.closePath();
    ctx.fillStyle = 'black';
};


// section: View

const getInput = (name) => document.querySelector(`input[name=${name}]`);

const getTranslationInput = (axis) => getInput(`translation${axis.toUpperCase()}`);

const getRotationInput = (axis) => getInput(`rotation${axis.toUpperCase()}`);

const getScaleInput = (axis) => getInput(`scale${axis.toUpperCase()}`);

const getTranslationValue = () => {
    const x = getTranslationInput('x').value;
    const y = getTranslationInput('y').value;
    const z = getTranslationInput('z').value;

    return [x, y, z];
};

const getRotationValue = () => {
    const x = getRotationInput('x').value;
    const y = getRotationInput('y').value;
    const z = getRotationInput('z').value;

    return [x, y, z];
};

const getScaleValue = () => {
    const x = getScaleInput('x').value;
    const y = getScaleInput('y').value;
    const z = getScaleInput('z').value;

    return [x, y, z];
};

const syncViewWithState = () => {
    for (const i in AXIS) {
        const currentAxis = AXIS[i];
        getScaleInput(currentAxis).value = state.scale[i];
        getTranslationInput(currentAxis).value = state.translation[i];
        getRotationInput(currentAxis).value = state.rotation[i];
    }
};

const onInputChange = (input, i, stateValue, j) => {
    const value = +input.value;
    let nextValue = stateValue;
    nextValue[i] = value;
    let arg = [undefined, undefined, undefined];
    arg[j] = nextValue;
    updateState.apply(this, arg);
    redraw();
};

const listenForm = () => {
    for (const i in AXIS) {
        const axis = AXIS[i];

        const scaleInput = getScaleInput(axis);
        scaleInput.addEventListener('change', (e) => {
            onInputChange(scaleInput, i, getScaleValue(axis), 2);
        });

        const translationInput = getTranslationInput(axis);
        translationInput.addEventListener('change', (e) => {
            onInputChange(translationInput, i, getTranslationValue(axis), 1);
        });

        const rotationInput = getRotationInput(axis);
        rotationInput.addEventListener('change', (e) => {
            onInputChange(rotationInput, i, getRotationValue(axis), 0);
        });
    }
};

const redraw = () => {
    getContext().clearRect(0, 0, canvas.width, canvas.height);

    let transform = getTransformMatFromState();
    let modelviewA = transformFace(faceA, transform);
    let modelviewB = transformFace(faceB, transform);
    let modelviewC = transformFace(faceC, transform);
    let modelviewD = transformFace(faceD, transform);
    syncViewWithState();
    let imageData = createImageData();
    let imageBuffer = createImageBuffer();

    imageBuffer = rasterize(imageBuffer, dehomoginizeTriangle(modelviewA), [255, 0, 0, 255]);
    imageBuffer = rasterize(imageBuffer, dehomoginizeTriangle(modelviewB), [0, 255, 0, 255]);
    imageBuffer = rasterize(imageBuffer, dehomoginizeTriangle(modelviewC), [0, 0, 255, 255]);
    imageBuffer = rasterize(imageBuffer, dehomoginizeTriangle(modelviewD), [255, 0, 255, 255]);

    imageData = putImageBufferToImageData(imageBuffer, imageData);

    drawImage(imageData);

    drawFace(modelviewA, 'red');
    drawFace(modelviewB, 'blue');
    drawFace(modelviewC, 'green');
};


// section: Main

const amazeMe = () => {
    let nextRotation = state.rotation;
    let nextScale = state.scale;
    let nextTranslation = state.translation;

    const randomize = (triple) => {
        let result = [];
        for (const value of triple) {
            result.push(value + Math.random() * (Math.random() < 0.5 ? -1 : 1));
        }
        return result;
    };

    const loop = () => {
        nextRotation = [(nextRotation[0] + 1) % 360, (nextRotation[1] - 1) % 360, (nextRotation[2] + 1) % 360];
        nextScale = randomize(nextScale);
        nextTranslation = randomize(nextTranslation);
        updateState(nextRotation, nextTranslation, nextScale);
        syncViewWithState();
        redraw();

        setTimeout(loop, 1000 / 30);
    };

    loop();
};

const main = () => {
    updateState([0, 0, 0], [200, 200, 0], [100, 100, 100]);
    //updateState([2, 72, 12], [200, 200, 0], [100, 100, 100]);
    listenForm();
    //amazeMe();
    redraw();
};

window.onload = main;

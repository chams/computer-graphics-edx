//
// Draw 2d plane
//

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <GLUT/glut.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glext.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "shaders.h"

using namespace std;

// TODO: custom shaders

// Define geometry

const GLfloat triangleVerts[3][3] = {
  {0.0, 0.5, 0.0}, {-0.5, -0.5, 0}, {0.5, -0.5, 0}
};

const GLfloat triangleColors[3][3] = {
  {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}
};

const GLubyte triangleInds[1][3] = {
  {0, 1, 2}
};

// Define data structures

const int numObjects = 1;
const int bufferPerObject = 3;
GLuint VAOs[numObjects]; // Store VAO id
GLuint buffers[numObjects * bufferPerObject];
GLuint objects[numObjects];
GLenum PrimType[numObjects];
GLsizei NumElems[numObjects];

enum {Vertices, Colors, Elements};
enum {TRIANGLE1};

// Variables
GLfloat eyeloc = 2.0;
GLuint vertexshader, fragmentshader, shaderprogram;
GLuint projectionPos, modelviewPos;
glm::mat4 projection, modelView;

// Initialization
// OK
void initObject(GLuint object, GLfloat* vert, GLint sizevert, GLfloat* color, GLint sizecol, GLubyte* inds, GLint sizeind, GLenum type) {
  int offset = object * bufferPerObject;

  // Vertices
  glBindVertexArray(VAOs[object]);
  glBindBuffer(GL_ARRAY_BUFFER, buffers[Vertices + offset]);
  glBufferData(GL_ARRAY_BUFFER, sizevert, vert, GL_STATIC_DRAW);
  // pass to shader
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);

  // Colors
  glBindBuffer(GL_ARRAY_BUFFER, buffers[Colors + offset]);
  glBufferData(GL_ARRAY_BUFFER, sizecol, color, GL_STATIC_DRAW);
  // pass to shader
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);

  // Elements 
  // TODO: where we use it?
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[Elements + offset]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeind, inds, GL_STATIC_DRAW);

  PrimType[object] = type;
  NumElems[object] = sizeind;

  //// Unbind
  glBindVertexArray(0);
}

// OK
void init(void) {
  glClearColor(0.0, 0.0, 0.0, 0.0);

  // TODO: custom projection matrix
  projection = glm::mat4(1.0f);
  // TODO: custom matrix
  modelView = glm::lookAt(glm::vec3(0, -eyeloc, eyeloc), glm::vec3(0, 0, 0), glm::vec3(0, 1, 1));

  glGenVertexArrays(numObjects, VAOs); // generate identifiers
  glGenBuffers(numObjects * bufferPerObject, buffers);

  initObject(
    TRIANGLE1,
    (GLfloat *) triangleVerts,
    sizeof(triangleVerts),
    (GLfloat *) triangleColors,
    sizeof(triangleColors),
    (GLubyte *) triangleInds,
    sizeof(triangleInds),
    GL_TRIANGLES
  );

  // init shaders
  vertexshader = initshaders(GL_VERTEX_SHADER, "shaders/nop.vert");
  fragmentshader = initshaders(GL_FRAGMENT_SHADER, "shaders/nop.frag");
  shaderprogram = initprogram(vertexshader, fragmentshader);
  projectionPos = glGetUniformLocation(shaderprogram, "projection");
  modelviewPos = glGetUniformLocation(shaderprogram, "modelview");
  glUniformMatrix4fv(projectionPos, 1, GL_FALSE, &projection[0][0]);
  glUniformMatrix4fv(modelviewPos, 1, GL_FALSE, &modelView[0][0]);
}

// OK
void deleteBuffers(void) {
  glDeleteVertexArrays(numObjects, VAOs);
  glDeleteBuffers(numObjects * bufferPerObject, buffers);
}

// OK
void drawobject(GLuint object) {
  glBindVertexArray(VAOs[object]);
  glDrawElements(PrimType[object], NumElems[object], GL_UNSIGNED_BYTE, 0);
  glBindVertexArray(0); // clear vertex array
}

// window callbacks

// OK
void display(void) {
  glClear(GL_COLOR_BUFFER_BIT);

  drawobject(TRIANGLE1);

  glFlush();
}

// OK
void reshape(int w, int h) {
  glViewport(0, 0, (GLsizei) w, (GLsizei) h);
  
  projection = glm::perspective(30.0f / 180.0f * glm::pi<float>(), (GLfloat)w / (GLfloat)h, 1.0f, 10.f);
  glUniformMatrix4fv(projectionPos, 1, GL_FALSE, &projection[0][0]);
}

// OK
int main(int argc, char** argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(500, 500);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("Chams draw!");

  init();

  glutDisplayFunc(display);
  glutReshapeFunc(reshape);

  glutMainLoop();
  deleteBuffers();

  return 0;
}

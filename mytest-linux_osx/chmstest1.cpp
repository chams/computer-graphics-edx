#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glext.h>
#include <GLUT/glut.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "shaders.h"

int mouseoldx, mouseoldy;
GLfloat eyeloc = 2.0;
GLuint vertexshader, fragmentshader, shaderprogram;
GLuint projectionPos, modelviewPos;
glm::mat4 projection, modelview;

const int numobjects = 2; // number of objects for buffer
const int numperobj = 3; // Vertices, colors, indices
GLuint VAOs[numobjects]; // A VAO for each object
GLuint buffers[numperobj * numobjects]; // List of buffers for geometric data
GLuint objects[numobjects];
GLenum PrimType[numobjects]; // Primitive types (quads, polygons etc)
GLsizei NumElems[numobjects]; // Number of geometric elements

enum {Vertices, Colors, Elements};
enum {FLOOR, FLOOR2};

// TODO: why this coords?
const GLfloat floorverts[4][3] = {
  {0.5, 0.5, 0.0}, {-0.5, 0.5, 0.0}, {-0.5, -0.5, 0.0}, {0.5, -0.5, 0.0}
};

const GLfloat floorcol[4][3] = {
  {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}, {1.0, 1.0, 1.0}
};

const GLubyte floorinds[1][6] = { {0, 1, 2, 0, 2, 3} };

const GLfloat floorverts2[4][3] = {
	{0.5, 0.5, 1.0}, {-0.5, 0.5, 1.0}, {-0.5, -0.5, 1.0}, {0.5, -0.5, 1.0}
} ; 
const GLfloat floorcol2[4][3] = {
	{1.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {1.0, 0.0, 0.0}
} ; 
const GLubyte floorinds2[1][6] = { { 0, 1, 2, 0, 2, 3 } } ;

void deleteBuffers() {
  glDeleteVertexArrays(numobjects, VAOs);
  glDeleteBuffers(numperobj * numobjects, buffers);
}

void drawobject(GLuint object) {
  glBindVertexArray(VAOs[object]);
  glDrawElements(PrimType[object], NumElems[object], GL_UNSIGNED_BYTE, 0);
  glBindVertexArray(0); // clear vertex array
}

void display(void) {
  glClear(GL_COLOR_BUFFER_BIT);

  drawobject(FLOOR);
  drawobject(FLOOR2);
  
  glFlush();
}

void reshape(int w, int h) {
  glViewport(0, 0, (GLsizei) w, (GLsizei) h);

  projection = glm::perspective(30.0f / 180.0f * glm::pi<float>(), (GLfloat)w / (GLfloat)h, 1.0f, 10.f);
  glUniformMatrix4fv(projectionPos, 1, GL_FALSE, &projection[0][0]);
}

void initobject(GLuint object, GLfloat* vert, GLint sizevert, GLfloat* col, GLint sizecol, GLubyte* inds, GLint sizeind, GLenum type) {
  int offset = object * numperobj;
  glBindVertexArray(VAOs[object]);
  glBindBuffer(GL_ARRAY_BUFFER, buffers[Vertices + offset]);
  glBufferData(GL_ARRAY_BUFFER, sizevert, vert, GL_STATIC_DRAW);

  // Use layout location 0 for vert
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
  glBindBuffer(GL_ARRAY_BUFFER, buffers[Colors + offset]);
  glBufferData(GL_ARRAY_BUFFER, sizecol, col, GL_STATIC_DRAW);
  
  // Use layout location 1 for colors
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[Elements + offset]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeind, inds, GL_STATIC_DRAW);
  PrimType[object] = type;
  NumElems[object] = sizeind;
  
  // Prevent futher modification of this VAO by unbinding it
  glBindVertexArray(0);
}

void init(void) {
  glClearColor(0.0, 0.0, 0.0, 0.0);

  projection = glm::mat4(1.0f);
  modelview = glm::lookAt(glm::vec3(0, -eyeloc, eyeloc), glm::vec3(0, 0, 0), glm::vec3(0, 1, 1));

  // Create the buffer objects to be used n the scene later
  glGenVertexArrays(numobjects, VAOs);
  glGenBuffers(numperobj * numobjects, buffers);

  initobject(
    FLOOR,
    (GLfloat *) floorverts,
    sizeof (floorverts),
    (GLfloat *) floorcol,
    sizeof (floorcol),
    (GLubyte *) floorinds, 
    sizeof (floorinds),
    GL_TRIANGLES
  );

  initobject(
    FLOOR2,
    (GLfloat *) floorverts2,
    sizeof(floorverts2),
    (GLfloat *) floorcol2,
    sizeof(floorcol2),
    (GLubyte *) floorinds2, 
    sizeof(floorinds2),
    GL_TRIANGLES
  );
  
  // init vertex shader programs
  vertexshader = initshaders(GL_VERTEX_SHADER, "shaders/nop.vert");
  fragmentshader = initshaders(GL_FRAGMENT_SHADER, "shaders/nop.frag");
  shaderprogram = initprogram(vertexshader, fragmentshader);
  // get the positions of the uniform variables
  projectionPos = glGetUniformLocation(shaderprogram, "projection");
  modelviewPos = glGetUniformLocation(shaderprogram, "modelview");
  // pass the projection and modelview matrices to the shader
  glUniformMatrix4fv(projectionPos, 1, GL_FALSE, &projection[0][0]);
  glUniformMatrix4fv(modelviewPos, 1, GL_FALSE, &modelview[0][0]);
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(500, 500);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("Simple chms demo with shaders");

  init();

  glutDisplayFunc(display);
  glutReshapeFunc(reshape);

  glutMainLoop();
  deleteBuffers();

  return 0;
}
